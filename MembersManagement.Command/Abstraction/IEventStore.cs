﻿using MembersManagement.Command.Domain;
using MembersManagement.Command.Events;

namespace MembersManagement.Command.Abstraction
{
    public interface IEventStore
    {

        Task<List<Event>> GetAllAsync(string aggregateId, CancellationToken cancellationToken);
        Task CommitAsync(Invitation invitation, CancellationToken cancellationToken);
    }
}
