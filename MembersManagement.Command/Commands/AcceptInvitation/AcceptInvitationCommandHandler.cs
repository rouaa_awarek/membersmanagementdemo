﻿using MediatR;
using MembersManagement.Command.Abstraction;
using MembersManagement.Command.Domain;
using MembersManagement.Command.Exceptions;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace MembersManagement.Command.Commands.AcceptInvitation
{
    public class AcceptInvitationCommandHandler(IEventStore eventStore) : IRequestHandler<AcceptInvitationCommand, string>
    {
        private readonly IEventStore _eventStore = eventStore;

        public async Task<string> Handle(AcceptInvitationCommand command, CancellationToken cancellationToken)
        {
            var events = await _eventStore.GetAllAsync(command.SubscriptionId + "-" + command.MemberId, cancellationToken);

            if (events.Count == 0)
                throw new NotFoundException("Invitations not found");

            
            var invitation = Invitation.LoadFromHistory(events);

            invitation.Accept(command);

            await _eventStore.CommitAsync(invitation, cancellationToken);

            return command.SubscriptionId + "-" + command.MemberId;
        }
    }
}
