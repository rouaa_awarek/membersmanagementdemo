﻿using MediatR;

namespace MembersManagement.Command.Commands.CancelInvitation
{
    public class CancelInvitationCommand:IRequest<string>
    {

        public required string UserId { get; init; }
        public required string AccountId { get; init; }
        public required string SubscriptionId { get; init; }
        public required string MemberId { get; init; }
    }
}
