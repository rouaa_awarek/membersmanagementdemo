﻿using MediatR;
using MembersManagement.Command.Abstraction;
using MembersManagement.Command.Commands.SendInvitation;
using MembersManagement.Command.Domain;
using MembersManagement.Command.Exceptions;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace MembersManagement.Command.Commands.RejectInvitation
{
    public class RejectInvitationCommandHandler(IEventStore eventStore) : IRequestHandler<RejectInvitationCommand, string>
    {
        private readonly IEventStore _eventStore = eventStore;

        public async Task<string> Handle(RejectInvitationCommand command, CancellationToken cancellationToken)
        {
            var events = await _eventStore.GetAllAsync(command.SubscriptionId + "-" + command.MemberId, cancellationToken);

            if (events.Count == 0)
                throw new NotFoundException("Invitations not found");


            var invitation = Invitation.LoadFromHistory(events);
            invitation.Reject(command);
            await _eventStore.CommitAsync(invitation, cancellationToken);

            return command.SubscriptionId + "-" + command.MemberId;
        }
    }
}
