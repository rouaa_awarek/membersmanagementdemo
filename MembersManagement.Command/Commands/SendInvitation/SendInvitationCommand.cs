﻿using MediatR;

namespace MembersManagement.Command.Commands.SendInvitation
{
    public class SendInvitationCommand:IRequest<string>
    {

        public required string UserId { get; init; }
        public required string AccountId { get; init; }
        public required string SubscriptionId { get; init; }
        public required string MemberId { get; init; }
        public required int Permission { get; init; }

    }

}
