﻿using MediatR;
using MembersManagement.Command.Abstraction;
using MembersManagement.Command.Domain;
using MembersManagement.Command.Exceptions;

namespace MembersManagement.Command.Commands.SendInvitation
{
    public class SendInvitationCommandHandler(IEventStore eventStore) : IRequestHandler<SendInvitationCommand, string>
    {
        private readonly IEventStore _eventStore=eventStore ;

        public async Task<string> Handle(SendInvitationCommand command, CancellationToken cancellationToken)
        {
            var events = await _eventStore.GetAllAsync((command.SubscriptionId + "-" + command.MemberId), cancellationToken);
            if (events.Any())

                throw new AlreadyExistsException("this invitation is already exist");

            var invitation = Invitation.Send(command);

            await _eventStore.CommitAsync(invitation, cancellationToken);

            return command.SubscriptionId + "-" + command.MemberId;
        }
    }
}
