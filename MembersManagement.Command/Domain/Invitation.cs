﻿using MembersManagement.Command.Abstraction;
using MembersManagement.Command.Commands.AcceptInvitation;
using MembersManagement.Command.Commands.CancelInvitation;
using MembersManagement.Command.Commands.RejectInvitation;
using MembersManagement.Command.Commands.SendInvitation;
using MembersManagement.Command.Events;
using MembersManagement.Command.Exceptions;
using MembersManagement.Command.Extensions;
using System.Security;

namespace MembersManagement.Command.Domain
{
    public class Invitation : Aggregate<Invitation>, IAggregate
    {

        public static Invitation Send(SendInvitationCommand command)
        {
            var @event = command.ToEvent();
            if(@event.UserId==@event.Data.MemberId)
            {
                throw new RuleViolationException("You can't send invitation to yourself  ");
            }
            var invitation = new Invitation();

            invitation.ApplyNewChange(@event);

            return invitation;
        }

        
        public void Mutate(InvitationSent @event)
        {
            IsSent=true;
            IsCanceled = false;
            IsAccepted = false;
        }

        public void Cancel(CancelInvitationCommand command)
        {
            if (!IsSent)
                throw new RuleViolationException("You can't cancel unsent invitation");

            var @event = command.ToEvent(NextSequence);

            ApplyNewChange(@event);
        }
        public void Mutate(InvitationCanceled @event)
        {
            IsCanceled = true;
            

        }

        // public bool IsDeleted { get; private set; }
        public bool IsSent { get; private set; }
        public bool IsCanceled { get; private set; }
        public bool IsAccepted { get; private set; }
    //    public bool IsRejected { get; private set; }
       




        public void Accept(AcceptInvitationCommand command)
        {
            if(!IsSent)
                throw new RuleViolationException("You can't accept unsent invitation");
            if (IsCanceled)
                throw new RuleViolationException("You can't accept canceled invitation");
            var @event = command.ToEvent(NextSequence);

            ApplyNewChange(@event);
        }
        public void Mutate(InvitationAccepted @event)
        {
            IsAccepted = true; 
            IsCanceled=false;

        }

       


        public void Reject(RejectInvitationCommand command)
        {
            if (!IsSent )
                throw new RuleViolationException("You can't reject unsent invitation");
            if (IsCanceled)
                throw new RuleViolationException("You can't reject canceled invitation");
            var @event = command.ToEvent(NextSequence);

            ApplyNewChange(@event);
        }
        public void Mutate(InvitationRejected @event)
        {
            IsAccepted = false;

        }


        protected override void Mutate(Event @event)
        {
            switch (@event)
            {
                case InvitationAccepted e:
                    Mutate(e);
                    break;
                case InvitationCanceled e:
                    Mutate(e);
                    break;
                case InvitationRejected e:
                    Mutate(e);
                    break;
                case InvitationSent e:
                    Mutate(e);
                    break;
            }
        }
    }
}
