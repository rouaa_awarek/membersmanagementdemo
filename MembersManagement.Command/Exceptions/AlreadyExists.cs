﻿namespace MembersManagement.Command.Exceptions
{
    public class AlreadyExistsException(string message) : Exception(message)
    {
    }
}
