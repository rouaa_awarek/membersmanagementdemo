﻿namespace MembersManagement.Command.Exceptions
{
    public class NotFoundException(string message) : Exception(message)
    {
    }
}
