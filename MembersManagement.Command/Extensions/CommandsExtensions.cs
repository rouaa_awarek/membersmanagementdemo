﻿using MembersManagement.Command.Commands.AcceptInvitation;
using MembersManagement.Command.Commands.CancelInvitation;
using MembersManagement.Command.Commands.RejectInvitation;
using MembersManagement.Command.Commands.SendInvitation;
using MembersManagement.Command.Protos;
using System.Security;

namespace MembersManagement.Command.Extensions
{
    public static class CommandsExtensions
    {


        public static SendInvitationCommand ToCommand(this SendInvitationRequest request)
           => new()
           { 
               UserId= request.UserId,
               AccountId= request.AccountId,
               SubscriptionId= request.SubscriptionId,
               MemberId= request.MemberId,
               Permission= (int)request.Permission,
           }; 
        
        public static RejectInvitationCommand ToCommand(this RejectInvitationRequest request)
           => new()
           { 
               UserId= request.UserId,
               AccountId= request.AccountId,
               SubscriptionId= request.SubscriptionId,
               MemberId= request.MemberId,
           }; 
        
        public static CancelInvitationCommand ToCommand(this CancelInvitationRequest request)
           => new()
           { 
               UserId= request.UserId,
               AccountId= request.AccountId,
               SubscriptionId= request.SubscriptionId,
               MemberId= request.MemberId,
           };
        public static AcceptInvitationCommand ToCommand(this AcceptInvitationRequest request)
           => new()
           { 
               UserId= request.UserId,
               AccountId= request.AccountId,
               SubscriptionId= request.SubscriptionId,
               MemberId= request.MemberId,
           };

    }
}
