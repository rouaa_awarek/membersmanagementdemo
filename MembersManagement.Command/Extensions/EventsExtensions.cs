﻿using MembersManagement.Command.Commands.AcceptInvitation;
using MembersManagement.Command.Commands.CancelInvitation;
using MembersManagement.Command.Commands.RejectInvitation;
using MembersManagement.Command.Commands.SendInvitation;
using MembersManagement.Command.Events;

namespace MembersManagement.Command.Extensions
{
    public static class EventsExtensions
    {

       
        public static InvitationAccepted ToEvent(this AcceptInvitationCommand command, int sequence) => new(
                AggregateId:command.SubscriptionId + "-" + command.MemberId,
                Sequence: sequence,
                DateTime: DateTime.UtcNow,
                Data: new InvitationAcceptedData(
                    AccountId: command.AccountId,
                    SubscriptionId: command.SubscriptionId,
                    MemberId: command.MemberId,
                    UserId:command.UserId
                   
                ),
                UserId: command.UserId,
                Version: 1
            );
        
        public static InvitationCanceled ToEvent(this CancelInvitationCommand command, int sequence) => new(
                AggregateId:command.SubscriptionId+"-"+command.MemberId,
                Sequence: sequence,
                DateTime: DateTime.UtcNow,
                Data: new InvitationCanceledData(
                    AccountId: command.AccountId,
                    SubscriptionId: command.SubscriptionId,
                    MemberId: command.MemberId,
                    UserId:command.UserId
                   
                ),
                UserId: command.UserId,
                Version: 1
            ); 
        public static InvitationRejected ToEvent(this RejectInvitationCommand command, int sequence) => new(
                AggregateId:command.SubscriptionId+"-"+command.MemberId,
                Sequence: sequence,
                DateTime: DateTime.UtcNow,
                Data: new InvitationRejectedData(
                    AccountId: command.AccountId,
                    SubscriptionId: command.SubscriptionId,
                    MemberId: command.MemberId,
                    UserId:command.UserId
                   
                ),
                UserId: command.UserId,
                Version: 1
            ); 
        public static InvitationSent ToEvent(this SendInvitationCommand command) => new(
                AggregateId:command.SubscriptionId + "-" + command.MemberId,
                Sequence: 1,
                DateTime: DateTime.UtcNow,
                Data: new InvitationSentData(
                    AccountId: command.AccountId,
                    SubscriptionId: command.SubscriptionId,
                    MemberId: command.MemberId,
                    UserId:command.UserId,
                    Permission: command.Permission
                   
                ),
                UserId: command.UserId,
                Version: 1
            );
    
    
    } 
   
    
}
