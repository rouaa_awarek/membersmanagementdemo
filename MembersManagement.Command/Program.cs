using MembersManagement.Command.Abstraction;
using MembersManagement.Command.Infrastructure.Persistence;
using MembersManagement.Command.Interceptors;
using MembersManagement.Command.Services;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
// Add services to the container.
builder.Services.AddGrpc(option =>
{
    option.Interceptors.Add<ApplicationExceptionInterceptor>();
}); 
builder.Services.AddMediatR(o => o.RegisterServicesFromAssemblyContaining<Program>());

builder.Services.AddDbContext<ApplicationDbContext>(options =>
options.UseSqlServer(connectionString));
builder.Services.AddScoped<IEventStore, EventStore>();
var app = builder.Build();

// Configure the HTTP request pipeline.
app.MapGrpcService<GreeterService>();
app.MapGrpcService<InvitationsService>();
app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

app.Run();
