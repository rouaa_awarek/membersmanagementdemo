// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: Protos/invitations.proto
// </auto-generated>
#pragma warning disable 0414, 1591, 8981, 0612
#region Designer generated code

using grpc = global::Grpc.Core;

namespace MembersManagement.Command.Protos {
  public static partial class Invitations
  {
    static readonly string __ServiceName = "members_management.command.v1.Invitations";

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static void __Helper_SerializeMessage(global::Google.Protobuf.IMessage message, grpc::SerializationContext context)
    {
      #if !GRPC_DISABLE_PROTOBUF_BUFFER_SERIALIZATION
      if (message is global::Google.Protobuf.IBufferMessage)
      {
        context.SetPayloadLength(message.CalculateSize());
        global::Google.Protobuf.MessageExtensions.WriteTo(message, context.GetBufferWriter());
        context.Complete();
        return;
      }
      #endif
      context.Complete(global::Google.Protobuf.MessageExtensions.ToByteArray(message));
    }

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static class __Helper_MessageCache<T>
    {
      public static readonly bool IsBufferMessage = global::System.Reflection.IntrospectionExtensions.GetTypeInfo(typeof(global::Google.Protobuf.IBufferMessage)).IsAssignableFrom(typeof(T));
    }

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static T __Helper_DeserializeMessage<T>(grpc::DeserializationContext context, global::Google.Protobuf.MessageParser<T> parser) where T : global::Google.Protobuf.IMessage<T>
    {
      #if !GRPC_DISABLE_PROTOBUF_BUFFER_SERIALIZATION
      if (__Helper_MessageCache<T>.IsBufferMessage)
      {
        return parser.ParseFrom(context.PayloadAsReadOnlySequence());
      }
      #endif
      return parser.ParseFrom(context.PayloadAsNewBuffer());
    }

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Marshaller<global::MembersManagement.Command.Protos.SendInvitationRequest> __Marshaller_members_management_command_v1_SendInvitationRequest = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::MembersManagement.Command.Protos.SendInvitationRequest.Parser));
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Marshaller<global::MembersManagement.Command.Protos.Response> __Marshaller_members_management_command_v1_Response = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::MembersManagement.Command.Protos.Response.Parser));
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Marshaller<global::MembersManagement.Command.Protos.CancelInvitationRequest> __Marshaller_members_management_command_v1_CancelInvitationRequest = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::MembersManagement.Command.Protos.CancelInvitationRequest.Parser));
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Marshaller<global::MembersManagement.Command.Protos.AcceptInvitationRequest> __Marshaller_members_management_command_v1_AcceptInvitationRequest = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::MembersManagement.Command.Protos.AcceptInvitationRequest.Parser));
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Marshaller<global::MembersManagement.Command.Protos.RejectInvitationRequest> __Marshaller_members_management_command_v1_RejectInvitationRequest = grpc::Marshallers.Create(__Helper_SerializeMessage, context => __Helper_DeserializeMessage(context, global::MembersManagement.Command.Protos.RejectInvitationRequest.Parser));

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Method<global::MembersManagement.Command.Protos.SendInvitationRequest, global::MembersManagement.Command.Protos.Response> __Method_SendInvitation = new grpc::Method<global::MembersManagement.Command.Protos.SendInvitationRequest, global::MembersManagement.Command.Protos.Response>(
        grpc::MethodType.Unary,
        __ServiceName,
        "SendInvitation",
        __Marshaller_members_management_command_v1_SendInvitationRequest,
        __Marshaller_members_management_command_v1_Response);

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Method<global::MembersManagement.Command.Protos.CancelInvitationRequest, global::MembersManagement.Command.Protos.Response> __Method_CancelInvitation = new grpc::Method<global::MembersManagement.Command.Protos.CancelInvitationRequest, global::MembersManagement.Command.Protos.Response>(
        grpc::MethodType.Unary,
        __ServiceName,
        "CancelInvitation",
        __Marshaller_members_management_command_v1_CancelInvitationRequest,
        __Marshaller_members_management_command_v1_Response);

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Method<global::MembersManagement.Command.Protos.AcceptInvitationRequest, global::MembersManagement.Command.Protos.Response> __Method_AcceptInvitation = new grpc::Method<global::MembersManagement.Command.Protos.AcceptInvitationRequest, global::MembersManagement.Command.Protos.Response>(
        grpc::MethodType.Unary,
        __ServiceName,
        "AcceptInvitation",
        __Marshaller_members_management_command_v1_AcceptInvitationRequest,
        __Marshaller_members_management_command_v1_Response);

    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    static readonly grpc::Method<global::MembersManagement.Command.Protos.RejectInvitationRequest, global::MembersManagement.Command.Protos.Response> __Method_RejectInvitation = new grpc::Method<global::MembersManagement.Command.Protos.RejectInvitationRequest, global::MembersManagement.Command.Protos.Response>(
        grpc::MethodType.Unary,
        __ServiceName,
        "RejectInvitation",
        __Marshaller_members_management_command_v1_RejectInvitationRequest,
        __Marshaller_members_management_command_v1_Response);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::MembersManagement.Command.Protos.InvitationsReflection.Descriptor.Services[0]; }
    }

    /// <summary>Base class for server-side implementations of Invitations</summary>
    [grpc::BindServiceMethod(typeof(Invitations), "BindService")]
    public abstract partial class InvitationsBase
    {
      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::System.Threading.Tasks.Task<global::MembersManagement.Command.Protos.Response> SendInvitation(global::MembersManagement.Command.Protos.SendInvitationRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::System.Threading.Tasks.Task<global::MembersManagement.Command.Protos.Response> CancelInvitation(global::MembersManagement.Command.Protos.CancelInvitationRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::System.Threading.Tasks.Task<global::MembersManagement.Command.Protos.Response> AcceptInvitation(global::MembersManagement.Command.Protos.AcceptInvitationRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
      public virtual global::System.Threading.Tasks.Task<global::MembersManagement.Command.Protos.Response> RejectInvitation(global::MembersManagement.Command.Protos.RejectInvitationRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

    }

    /// <summary>Creates service definition that can be registered with a server</summary>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    public static grpc::ServerServiceDefinition BindService(InvitationsBase serviceImpl)
    {
      return grpc::ServerServiceDefinition.CreateBuilder()
          .AddMethod(__Method_SendInvitation, serviceImpl.SendInvitation)
          .AddMethod(__Method_CancelInvitation, serviceImpl.CancelInvitation)
          .AddMethod(__Method_AcceptInvitation, serviceImpl.AcceptInvitation)
          .AddMethod(__Method_RejectInvitation, serviceImpl.RejectInvitation).Build();
    }

    /// <summary>Register service method with a service binder with or without implementation. Useful when customizing the service binding logic.
    /// Note: this method is part of an experimental API that can change or be removed without any prior notice.</summary>
    /// <param name="serviceBinder">Service methods will be bound by calling <c>AddMethod</c> on this object.</param>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    [global::System.CodeDom.Compiler.GeneratedCode("grpc_csharp_plugin", null)]
    public static void BindService(grpc::ServiceBinderBase serviceBinder, InvitationsBase serviceImpl)
    {
      serviceBinder.AddMethod(__Method_SendInvitation, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::MembersManagement.Command.Protos.SendInvitationRequest, global::MembersManagement.Command.Protos.Response>(serviceImpl.SendInvitation));
      serviceBinder.AddMethod(__Method_CancelInvitation, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::MembersManagement.Command.Protos.CancelInvitationRequest, global::MembersManagement.Command.Protos.Response>(serviceImpl.CancelInvitation));
      serviceBinder.AddMethod(__Method_AcceptInvitation, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::MembersManagement.Command.Protos.AcceptInvitationRequest, global::MembersManagement.Command.Protos.Response>(serviceImpl.AcceptInvitation));
      serviceBinder.AddMethod(__Method_RejectInvitation, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::MembersManagement.Command.Protos.RejectInvitationRequest, global::MembersManagement.Command.Protos.Response>(serviceImpl.RejectInvitation));
    }

  }
}
#endregion
